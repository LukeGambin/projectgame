﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    public void LoadLevel(string name)
    {

        print("Loading Level" + name);

        //Loads the scene name
        SceneManager.LoadScene(name);
    }

    public void QuitEXE()
    {
        Application.Quit();
    }

    public void QuitEditor()
    {
        UnityEditor.EditorApplication.isPlaying = false;
    }
}