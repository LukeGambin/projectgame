﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour {

    public GameObject bulletPrefab;

    public float shootDelay = 0.25f; //Set a delay between firing one bullet and an other
    float coodownTimer = 0; 
	
	// Update is called once per frame
	void Update () {
        coodownTimer -= Time.deltaTime;

        if(Input.GetButton("Fire1") && coodownTimer <= 0) //If the shootdelay is 0 shoot 
        {
            Debug.Log("Shooting");
            coodownTimer = shootDelay;

            Vector3 offset = new Vector3(0, 0.8f, 0); //Setting the offset to a 0.8 points away from the player plane
            Instantiate(bulletPrefab, transform.position + offset, transform.rotation); //Spawn the bullet infront of the plane to not cause collision and suicide
        }
	}
}
