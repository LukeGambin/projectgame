﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathSceneChange : MonoBehaviour
{
    public int hp;

    void OnTriggerEnter2D()
    {

        hp--;

        if (hp <= 0)
        {
            SceneManager.LoadScene("Death");
        }
    }
}
