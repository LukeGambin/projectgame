﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float moveSpeed = 5; //setting the max speed of the playerplane

    float padding = 0.5f;
    float xMin, xMax;
    float yMin, yMax;

	void Start () {
        MapBoundaries();
	}
	
	void Update () {
        Move();
	}

    void MapBoundaries()
    {
        Camera gameCamera = Camera.main;

        xMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).x + padding;
        xMax = gameCamera.ViewportToWorldPoint(new Vector3(1, 0, 0)).x - padding;

        yMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).y + padding;
        yMax = gameCamera.ViewportToWorldPoint(new Vector3(0, 1, 0)).y - padding;
    }

    private void Move()
    {
        //var is a generic variable which changes its type
        //depending on the variable
        //deltaX: saves the differance travelled in x-axis
        var deltaX = Input.GetAxis("Horizontal") * Time.deltaTime * moveSpeed;
        var deltaY = Input.GetAxis("Vertical") * Time.deltaTime * moveSpeed;

        //newXPos: saves the new position
        //Clamps the position of the spaceship between xMin and xMax
        var newXPos = Mathf.Clamp(this.transform.position.x + deltaX, xMin, xMax);
        var newYPos = Mathf.Clamp(this.transform.position.y + deltaY, yMin, yMax);

        transform.position = new Vector2(newXPos, newYPos);
    }
}
