﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour {

    public float moveSpeed = 5; //Setting the move speed for the bullet

    // Update is called once per frame
    void Update () {

        //Same code for player movement
        Vector3 pos = transform.position; 
        pos.y += moveSpeed * Time.deltaTime;
        transform.position = pos;
    }
}
