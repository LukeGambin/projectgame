﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    public float currentTime = 0f;
    public float countdownTime = 30f;

    [SerializeField] Text countdownText;

    void Start()
    {
        currentTime = countdownTime;
    }

    void Update()
    {
        currentTime -= 1 * Time.deltaTime; //Decrease the timer by 1
        countdownText.text = currentTime.ToString("0"); //Show only the whole numbers

        if (currentTime <= 0)
        {
            currentTime = 0; //Set the time to 0 to avoid going lower than 0 (-1 -2 -3)
            SceneManager.LoadScene("Start2");
        }
    }
	
}
