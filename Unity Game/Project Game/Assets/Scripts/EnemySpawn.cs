﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour {

    public GameObject enemy;
    float randX;
    Vector3 spawnLocation;
    public float spawnRate = 2;
    float nextSpawn = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
        if(Time.time > nextSpawn)
        {
            nextSpawn = Time.time + spawnRate;
            randX = Random.Range(-4.6f, 6.7f);
            spawnLocation = new Vector3(randX, transform.position.y);
            Instantiate(enemy, spawnLocation, Quaternion.identity);
        }

	}
}
