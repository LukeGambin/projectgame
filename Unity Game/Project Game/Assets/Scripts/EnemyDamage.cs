﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamage : MonoBehaviour
{

    public GameObject effect;
    public int health;

    void OnTriggerEnter2D()
    {
        health--;

        if (health <= 0)
        {
            ScoreSystem.scoreValue += 50;
            Instantiate(effect, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }

    }
}
